function clock() {
    let time = new Date();

    // get day in string
    let day;
    switch (time.getDay()) {
        case 0:
            day = "Sunday";
            break;
        case 1:
            day = "Monday";
            break;
        case 2:
            day = "Tuesday";
            break;
        case 3:
            day = "Wednesday";
            break;
        case 4:
            day = "Thursday";
            break;
        case 5:
            day = "Friday";
            break;
        case 6:
            day = "Saturday";
    }

    //get month in string
    let month;
    switch (time.getMonth()) {
        case 0:
            month = "Jan";
            break;
        case 1:
            month = "Feb";
            break;
        case 2:
            month = "Mar";
            break;
        case 3:
            month = "Apr";
            break;
        case 4:
            month = "May";
            break;
        case 5:
            month = "Jun";
            break;
        case 6:
            month = "Jul";
            break;
        case 7:
            month = "Aug";
            break;
        case 8:
            month = "Sep";
            break;
        case 9:
            month ="Oct";
            break;
        case 10:
            month = "Nov";
            break;
        case 11:
            month = "Dec";
    }

    let clock = time.getHours() + " : " + time.getMinutes() + " : " + time.getSeconds() ;
    let date = day + "  " + time.getDate() + " " + month + " " + time.getFullYear();

    // UTC Time Zone
    let utcMin;
    let utcHour;

    // if time zone offset < 1 min => show it
    if (Math.abs(time.getTimezoneOffset()) < 60){
        utcMin = time.getTimezoneOffset();
    } 
    // else, if time zone offset is negative 
    else if (time.getTimezoneOffset() > 0) {
        utcHour = Math.floor(time.getTimezoneOffset() / 60);
        utcMin = time.getTimezoneOffset() - (utcHour*60);
    } 
    // time zone offset is passitive
    else {
        utcHour = Math.ceil(time.getTimezoneOffset() / 60); 
        utcMin = Math.abs(time.getTimezoneOffset() - (utcHour*60)); 
    }
    let utc = utcHour + ":" + utcMin + " UTC";

    console.log(clock + "\n" + date+ " " + utc);

    //set timeout for reload page in evary 1 sec.
    setTimeout(function(){
        window.location.reload(1);
    }, 1000);
}

export default clock();
